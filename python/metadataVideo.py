from .FFmpegCoreTools import FFmpegCoreTools

""" 
Sample config dictionary: 

ffmpeg_path = os.path.join(
        os.path.dirname(
            os.path.abspath(__file__)
        ),
        "ffmpeg",
        "bin",
        "ffmpeg.exe"
    )

    input_file = \
        os.path.join(
            os.path.dirname(
                os.path.abspath(__file__)
            ),
            "test.mov"
        )

    logo_file = \
        os.path.join(
            os.path.dirname(
                os.path.abspath(__file__)
            ),
            "images",
            "mighty-logo.png"
        )

    output_file = \
        os.path.join(
            os.path.dirname(
                os.path.abspath(__file__)
            ),
            "output.mov"
        )

    #   . . . . . . . . . . . . . . . . . . . . . .

    meta_data_text = \
        "drawtext=fontfile=fonts/Helvetica.ttf" + \
        ":fontsize=16" + \
        ":fontcolor=white" + \
        ":text=" + \
        "\"" + \
        "Code  |  {0}\n".format("value") + \
        "Focal Length  |  {0}\n".format("value") + \
        "Focus Distance  |  {0}\n".format("value") + \
        "F-Stop  | {0}".format("value") + \
        "\"" + \
        ":x=((1450-1280)/2):y=720+25"

    #   . . . . . . . . . . . . . . . . . . . . . .

    config = {
        "overwrite": {
            "value": True
        },
        "input-0": {
            "value": input_file
        },
        "input-1": {
            "value": logo_file
        },
        "filter_complex": {
            "value":
            "[0]pad=1450:820:((1450-1280)/2):20:black[playblast];" +
            "[1]scale=80:80[logo];" +
            "[playblast][logo]overlay=1450-85:820-80[video];" +
            "[video]{0}".format(meta_data_text)
        },
        "vcodec": {
            "value": "libx264"
        },

        "acodec": {
            "value": "aac"
        },
        "size": {
            "value": "1450x820"
        },
        "output": {
            "value": output_file
        }
    } 
    """


class MetadataVideo(object):
    def __init__(self):
        self.core_tools = FFmpegCoreTools()
        self.ffmpeg = self.core_tools.get_bin_path()

    def command(self, list_of_flags):
        command_flags = [self.ffmpeg]

        for element in list_of_flags:

            if element["name"] == "overwrite":
                if element["value"]:
                    command_flags.append("-y")
            else:
                flag = ""

                if "input" in element["name"]:
                    flag = "-i"
                elif element["name"] == "framerate":
                    flag = "-f"
                elif element["name"] == "size":
                    flag = "-s"
                elif element["name"] == "output":
                    flag = ""
                elif not element["name"].startswith("-"):
                    flag = "-" + element["name"]
                else:
                    flag = element["name"]

                if isinstance(element["value"], bool):
                    if element["value"]:
                        command_flags.append(flag)
                else:
                    command_flags.append(flag)
                    command_flags.append(str(element["value"]))

        return " ".join(command_flags)
