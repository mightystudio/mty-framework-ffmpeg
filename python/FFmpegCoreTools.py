#####################################################################################
#
# Copyright (c) 2020 Mighty Animation Studio and Metacube Technology Entertainment
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided as part of a colaboration and subject to the
# Mighty - Metacube Pipeline Code Agreement and your personal work agreement.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the the specific terms of your contract. All rights
# not expressly granted therein are reserved by Mighty Animation Studio and
# Metacube Technology entertainment.
#
#####################################################################################

import platform
import os 
import subprocess
import traceback

class FFmpegCoreTools():

        def __init__(self):
            """ Initialize the class with os context """

            # Main binary file name
            self._binName = 'ffmpeg'
            # The binary file path 
            self._bin = self.get_bin()


        def set_binary(self, binary='convert'):
            """ Toggles the binary file to be executed and generates the corresponding path.

            :param binary: Selects betweeen two values, covert and media

            :return: None.
            """
            if binary == 'convert':
                self._binName = 'ffmpeg'
            elif binary == 'media':
                self._binName = 'ffprobe'
            else:
                self._binName = 'ffmpeg'
            # Update the path for the binary file
            self._bin = self.get_bin()


        def get_bin_version(self):
            """ Output the current version for selected command

            :return: The version string for the command or the output error result.
            """
            err, inf = self.execute_command('-version')
            if not err:
                version_result = inf.split('\n')[0]
            else:
                version_result = inf

            return version_result


        def get_bin_path(self):
            """return the binary file path"""
            return self._bin


        def get_os_path(self, os_folder):
            """ Builds the path to the binary file based on the os system
                parameter: 
                    os_folder: The folder name to user for the os.

                returns:
                    return: String path constructed.
            """
            root_folder = os.path.dirname(os.path.dirname(__file__))
            bin_folder = os.path.join(root_folder, 'bin', os_folder, self._binName)
            
            return bin_folder


        def get_bin(self):
            """ Selects the platform OS path for the binary file.

            :return:  String path to the finary file.
            """
            if 'linux' in platform.system().lower():
                return self.get_os_path('lnx')
            elif 'windows' in platform.system().lower():
                return self.get_os_path('win')
            else:
                return self.get_os_path('mac')


        def __handle_subprocess(self, exc_cmd):
            """ Handles the execution of a command via subpricess

            :param exc_cmd: Is a String or a list of string items to be used as the execution comnmand
            :return: Two values, the first is a boolean representing the result of the command using 0 or False if the
            was no error in the execution and 1 or True if an erroer was found and the second returned parameter is
            the error output string found.
            """

            etype = None
            try:
                process = subprocess.Popen(exc_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                out, err = process.communicate()
                if process.returncode:
                    # Tag for a command error result
                    etype = 'cmd'
            except:
                # Exception for any execution problemn within subprocess and the os.
                out = traceback.format_exc()
                # Tag for a os execution error
                etype = 'exc'

            # Format the output error result based on the type of error found.
            if etype is not None:
                if etype == 'cmd':
                    erroutput = 'Command execution error:\n{}'.format(out)
                else:
                    erroutput = 'Module execution error:\n{}'.format(out)
            else:
                erroutput = out

            return process.returncode, erroutput


        def select_method(self, argsvar):
            """ Select the function to be executed based on the type of parameters provided.

            :param argsvar: The variable must be string or list of strings
            :return:  The selected function to use.
            """
            fn = None
            if argsvar.__class__.__name__ == 'str':
                # select the string execution
                fn = self.execute_command_str
            elif argsvar.__class__.__name__ == 'list':
                non_strings = [item for item in argsvar if item.__class__.__name__ != 'str']
                # validate method only if the is strins in the list pf arguments.
                if not non_strings:
                    # select the list execution
                    fn = self.execute_command_list

            return fn


        def execute_command(self, cmd_args):
            """ Selects the corres

            :param cmd_args:
            :return:
            """
            make_operation = self.select_method(cmd_args)

            if make_operation is not None:
                error, info = make_operation(cmd_args)
            else:
                error = True
                info = 'Parameters provided not supported. Use only String or a list of Strings.'
                raise Exception('Paramters error:', 'The paramters used must be a Strig or a List of Strings')

            return error, info


        def  execute_command_list(self, arg_list=[]):
            """ Executes the ffmpeg comand with provided arguments true a subprocees thread.

                arg_str: A string containting the arguments to be executed by the command.
                         Default value: Is empty string, no arguments to execute.

                returns: The first parameter is a boolean for the result of the coomand execution,
                The second parameter is the output message for the commnd.
            """

            cmd = [self.get_bin_path()]
            cmd.extend(arg_list)

            result, output = self.__handle_subprocess(cmd)

            return result, output


        def  execute_command_str(self, arg_str=''):
            """ Executes the ffmpeg comand with provided arguments true a subprocees thread.

                arg_str: A string containting the arguments to be executed by the command.
                         Default value: Is empty string, no arguments to execute.

                returns: The first parameter is a boolean for the result of the coomand execution,
                The second parameter is the output message for the commnd.
            """
            bin_path = self.get_bin_path()
            if 'windows' in platform.system().lower():
                if ' ' in bin_path:
                    bin_path = '\"{}\"'.format(bin_path)

            cmd = '{} {}'.format(bin_path, arg_str)

            result, output = self.__handle_subprocess(cmd)

            return result, output